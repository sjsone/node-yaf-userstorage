# *Yet Another F@#%ing* UserStorage

A simple UserStorage for NodeJS-Projects. Currently only for macOS, but can be easily expanded. 

### Getting started

`````js
const {UserStorages} = require('yaf-userstorage')
const us = new UserStorages('NameOfYourApplication')
const storeSettings = us.create("Settings")
storeSettings.data.asdf = "test"
storeSettings.save()
`````

