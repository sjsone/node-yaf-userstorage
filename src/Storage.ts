import NodeFs, { PathLike } from "fs";

interface Store {
    data: any,
    info: {
        loaded: Date,
        saved: Date
    }
}

export default class Storage {
    protected path: PathLike;
    protected store: Store;

    constructor(path: PathLike, load:boolean = false) {
        this.path = path;
        
        this.createFileIfNotExists();

        this.store = {
            data: {},
            info: {
                loaded: new Date(),
                saved: new Date()
            }
        }

        if(load) {
            this.load();
        }
    }

    protected createFileIfNotExists() {
        if(!NodeFs.existsSync(this.path)){
            NodeFs.writeFileSync(this.path, JSON.stringify({}))
        }
    }

    public load() {
        const data = JSON.parse(NodeFs.readFileSync(this.path).toString())
        this.store.data = data;
        this.store.info.loaded = new Date()
    }

    public save() {
        return new Promise((resolve, reject) => {
            NodeFs.writeFile(this.path, JSON.stringify(this.store.data), (err) => {
                if(err) {
                    reject(err)
                }
                this.store.info.saved = new Date()
                resolve()
            })
        })
    }

    get data() {
        return this.store.data
    }

    set data(_) {
        
    }
}