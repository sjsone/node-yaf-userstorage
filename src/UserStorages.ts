import NodePath from 'path'
import Storage from './Storage'
import UserPath from './UserPath'

export default class UserStorages {
    protected basePath: string;

    public constructor(name: string) {
        this.basePath = UserPath.createApplicationPath(name)
    }

    protected buildFileName(name: string) {
        const fileNameB64 = Buffer.from(name).toString('base64')
        return fileNameB64.replace(/\+/g, '-').replace(/\//g, '_').replace(/\=+$/, '');
    }

    protected readFileName(fileNameB64: string) {
        if (fileNameB64.length % 4 != 0){
            fileNameB64 += ('===').slice(0, 4 - (fileNameB64.length % 4));
        }
        fileNameB64 = fileNameB64.replace(/-/g, '+').replace(/_/g, '/')
        return Buffer.from(fileNameB64, "base64").toString();
    }

    protected buildFilePath(fileName: string) {
        return NodePath.join(this.basePath, fileName);
    }

    public create(name: string) {
        const fileName = this.buildFileName(name);
        const filePath = this.buildFilePath(fileName);
        return new Storage(filePath)
    }
}


