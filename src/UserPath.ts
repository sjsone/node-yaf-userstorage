import NodePath from 'path'
import NodeFs from 'fs'
import NodeOs from 'os'

export default class UserPath {
    public static getApplicationPath(name:string, global = false) {
        let appPath = ''
        switch(process.platform) {
            case 'darwin': 
                appPath = global ? '/Library/Application Support/' : NodePath.join(NodeOs.homedir(), 'Library/Application Support/')
                break;
            default: 
                throw new Error('UserPath: <'+process.platform+'> is not compatible')
        }

        appPath = NodePath.join(appPath, name)
        return NodePath.resolve(appPath)
    }

    public static createApplicationPath(name:string, global = false) {
        const applicationPath = UserPath.getApplicationPath(name, global)
        if(!NodeFs.existsSync(applicationPath)) {
            NodeFs.mkdirSync(applicationPath)
        }
        return applicationPath
    }
}