import UserStorages from './UserStorages'
import Storage from './Storage'

export {
    UserStorages,
    Storage
}